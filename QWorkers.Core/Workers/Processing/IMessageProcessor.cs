﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Workers.Processing
{
    public interface IMessageProcessor<TMessage>
    {
        /// <summary>
        /// Should be called once before processing messages (e.g open any connections)
        /// </summary>
        void OnStart();

        /// <summary>
        /// Processes an individual message item
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        bool Process(TMessage message);

        /// <summary>
        /// Should be called once after processing ends (e.g close any connections etc.)
        /// </summary>
        void OnEnd();
    }
}
