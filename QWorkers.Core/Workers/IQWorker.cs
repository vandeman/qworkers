﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Workers
{
    public interface IQWorker: ICloneable
    {
        /// <summary>
        /// Even fired once when the worker starts
        /// </summary>
        event Action<IQWorker> WorkerStarted;

        /// <summary>
        /// Event fired when any exception occurs in the processing of an item
        /// </summary>
        event Action<IQWorker, Exception> ProcessingException;

        /// <summary>
        /// Fires just before the worker finished processing
        /// </summary>
        event Action<IQWorker> WorkerCompleted;

        /// <summary>
        /// Starts processing of the queue
        /// </summary>
        void Start();

        //Ends the processing of the queue
        void End();

    }
}
