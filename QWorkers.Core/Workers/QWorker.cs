﻿using QWorkers.Core.Management.LoopManagement;
using QWorkers.Core.Queues;
using QWorkers.Core.Workers.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Workers
{
    public class QWorker<T> : IQWorker
    {
        public event Action<IQWorker> WorkerStarted;

        public event Action<IQWorker, Exception> ProcessingException;

        public event Action<IQWorker> WorkerCompleted;

        private IMessageProcessor<T> processor;
        private IReadonlyQueue<T> queue;
        private ILoopManager loopManager;

        public QWorker(IMessageProcessor<T> processor, IReadonlyQueue<T> queue)
        {
            this.processor = processor;
            this.queue = queue;
            this.loopManager = new LimitedLoopManager(1000);
        }

        public QWorker(IMessageProcessor<T> processor, IReadonlyQueue<T> queue, ILoopManager loopManager)
        {
            this.processor = processor;
            this.queue = queue;
            this.loopManager = loopManager;
        }

        /// <summary>
        /// Starts the processing of the queue in another thread
        /// </summary>
        public void Start()
        {
            processor.OnStart();
            this.Started();

            var processingTask = Task.Factory.StartNew(() =>
            {
                while (loopManager.ShouldContinue)
                {
                    var messsageItem = queue.Dequeue();
                    if (messsageItem != null)
                    {
                        try
                        {
                            processor.Process(messsageItem);
                        }
                        catch(Exception e)
                        {
                            this.Exception(e);
                        }
                        
                    }
                }
                this.Completed();

            });
            
        }

        public void End()
        {
           //Call a method on the loop manager
            loopManager.Cancel();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
        void Started()
        {
            if (WorkerStarted != null)
            {
                WorkerStarted(this);
            }
        }

        void Exception(Exception e)
        {
            if (ProcessingException != null)
            {
                ProcessingException(this, e);
            }
        }

        void Completed()
        {
            if (WorkerCompleted != null)
            {
                WorkerCompleted(this);
            }
        }


    }
}
