﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Queues
{
    public class MemoryQueue<T> : IMyQueue<T>
    {
        ConcurrentQueue<T> memQ; //thread-safe FIFO queue

        public MemoryQueue()
        {
            memQ = new ConcurrentQueue<T>();
        }
        public void Enqueue(T item)
        {
            memQ.Enqueue(item);
        }

        public T Dequeue()
        {
            T item = default(T);
            memQ.TryDequeue(out item);
            return item;
        }

        public int Count
        {
            get { return memQ.Count; }
        }
}
}
