﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Queues
{
    interface IMyQueue
    {
        int Count { get; }
    }
    interface IMyQueue<T> : IReadonlyQueue<T>, IMyQueue 
    {
        void Enqueue(T item);

    }
}
