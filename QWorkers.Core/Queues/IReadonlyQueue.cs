﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Queues
{
    /// <summary>
    /// Split queue methods to read and write
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IReadonlyQueue<T>
    {
        T Dequeue();
         
    }
}
