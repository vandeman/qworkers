﻿using QWorkers.Core.Queues;
using QWorkers.Core.Utility.Buffers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Reporting
{
    public class QueueMetrics
    {

        private HistoricalBuffer<int> queueHistory;
        private IMyQueue monitoredQueue;

        int Delta { get { return 1; } }

        QueueMetrics(IMyQueue queue)
        {
            this.monitoredQueue = queue;
        }

        void SampleQueue()
        {
            queueHistory.Add(monitoredQueue.Count);
        }

        void Average()
        {
            //Get the average of the queue history
            var sum = 0;
            for (int i = 0; i < queueHistory.Count; i++)
            {
                sum += queueHistory.Item(i);
            }

            var average = sum / queueHistory.Count;
        }



    }
}
