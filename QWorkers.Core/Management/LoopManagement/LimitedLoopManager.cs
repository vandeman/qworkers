﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Management.LoopManagement
{
    public class LimitedLoopManager : ILoopManager
    {
        private int loopLimit;
        private int loopCount;

        public LimitedLoopManager(int loopLimit)
        {
            this.loopLimit = loopLimit;
        }

        public bool ShouldContinue
        {
            get
            {
                var continueReturn = false;
                loopCount++;

                if (loopCount <= loopLimit)
                {
                    continueReturn = true;
                }
                return continueReturn;
            }
        }


        public void Cancel()
        {
            loopCount = loopLimit;
        }
    }
}
