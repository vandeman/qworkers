﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Management.LoopManagement
{
    /// <summary>
    /// Class to allow worker to continue processing
    /// </summary>
    public interface ILoopManager
    {
        /// <summary>
        /// Returns true if the worker should continue processing
        /// </summary>
        bool ShouldContinue { get; }

        /// <summary>
        /// Tells the manager that should continue should return false from now on
        /// </summary>
        void Cancel();

    }
}
