﻿using QWorkers.Core.Management.WorkerScalingRules;
using QWorkers.Core.Queues;
using QWorkers.Core.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Management
{
    
     class WorkerManager
    {
        private IQWorkerScalingRule scalingRule;
        private IQWorker worker;
        private IMyQueue queue;

        private Queue<IQWorker> workers;

       

        internal  WorkerManager(IQWorkerScalingRule scalingRule, IQWorker worker, IMyQueue queue)
        {
            this.scalingRule = scalingRule;
            this.worker = worker;
            this.queue = queue;

            workers = new Queue<IQWorker>();
        }

        void Run()
        {
            Init();
            for(int i = 0; i< 100; i++)
            {
                MonitorSweep();
            }

        }

        /// <summary>
        /// Performs pre-run setup and checks
        /// </summary>
        void Init()
        {
            var firstWorker = (IQWorker)worker.Clone();
            workers.Enqueue(firstWorker);
            firstWorker.Start();
        }

        void MonitorSweep()
        {
            if (scalingRule.Increase(queue.Count))
            {
                var newWorker = (IQWorker)worker.Clone();
                workers.Enqueue(newWorker);
                newWorker.Start();
            }
            else if (scalingRule.Decrease(queue.Count))
            {
                var terminalWorker = workers.Dequeue();
                terminalWorker.End();
            }
        }

        int WorkerCount { get { return workers.Count; } }

    }
}
