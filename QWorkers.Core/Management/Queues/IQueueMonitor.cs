﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Management.Queues
{
    /// <summary>
    /// Represents a monitor of a queue item size
    /// </summary>
    interface IQueueMonitor
    {
        /// <summary>
        /// Determines the number of workers that should be processing the queue
        /// </summary>
        /// <param name="qItemCount">The current count of items in the queue</param>
        /// <returns>The number of workers required to process the queue</returns>
        int WorkerCount(int qItemCount);
        
    }
}
