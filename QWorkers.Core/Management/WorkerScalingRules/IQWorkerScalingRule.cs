﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Management.WorkerScalingRules
{
    /// <summary>
    /// Checks on the queue size and determines whether an increase or decrease in the number of workers required to preoces the queue
    /// </summary>
    public interface IQWorkerScalingRule
    {
        /// <summary>
        /// Determines whether the rule recommends an increase in the amount of workers
        /// </summary>
        /// <param name="qItemCount">The current number of items in the queue</param>
        /// <returns>True if the class deems more workers are needed</returns>
        bool Increase(int qItemCount);

        /// <summary>
        /// Determines whether the rule recommends a decrease in the amount of workers
        /// </summary>
        /// <param name="qItemCount"></param>
        /// <returns></returns>
        bool Decrease(int qItemCount);

    }
}
