﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Utility.Buffers
{
    class HistoricalBuffer<T>
    {
        ConcurrentQueue<T> queue;
        private int capacity;

        public HistoricalBuffer(int capacity)
        {
            this.capacity = capacity;
            queue = new ConcurrentQueue<T>();
        }

        public void Add(T item)
        {
            if (queue.Count == capacity)
            {
                T itemOut = default(T);
                queue.TryDequeue(out itemOut);
            }

            queue.Enqueue(item);

            

        }

        public int Count { get { return queue.Count; } }

        public T Item(int index)
        {
            var array = queue.ToArray();
            return array[index];
        }

    }
}
