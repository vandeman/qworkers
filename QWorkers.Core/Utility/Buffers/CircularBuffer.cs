﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Core.Utility
{
    public class CircularBuffer<T>
    {
        T[] buffer;
        int nextFree;

        public CircularBuffer(int length)
        {
            buffer = new T[length];
            nextFree = 0;
        }

        public void Add(T o)
        {
            buffer[nextFree] = o;
            nextFree = (nextFree + 1) % buffer.Length;
        }

        public int Count { get { return buffer.Count(); } }

        public T Item(int index)
        {
            return buffer[index];
        }
    }
}
