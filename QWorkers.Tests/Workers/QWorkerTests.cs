﻿using Moq;
using NUnit.Framework;
using QWorkers.Core.Queues;
using QWorkers.Core.Workers;
using QWorkers.Core.Workers.Processing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Tests.Workers
{
    [TestFixture]
    public class QWorkerTests
    {

        Mock<IMessageProcessor<object>> mockProcessor;
        Mock<IReadonlyQueue<object>> mockQueue;

        [SetUp]
        public void Setup() {
            mockProcessor = new Mock<IMessageProcessor<object>>();
            mockQueue = new Mock<IReadonlyQueue<object>>();
        }

        [Test]
        public void ProcessorOnStartIsCalled()
        {
            //Arrange
            var worker = new QWorker<object>(mockProcessor.Object, null);

            //Act
            worker.Start();

            //Assert
            mockProcessor.Verify(p => p.OnStart(), Times.Once);

        }

        [Test]
        public void CloneCopiesEventSubscribers()
        {
            //Arrange
            var worker = new QWorker<object>(mockProcessor.Object, null);

            var onStartEventFired = false;

            var startDelegate = new Action<IQWorker>(q => { onStartEventFired = true; });

            worker.WorkerStarted += startDelegate;

            var clonedWorker = (QWorker<object>)worker.Clone();

            //Act
            clonedWorker.Start();

            //Assert
            Assert.IsTrue(onStartEventFired);


        }

        [Test]
        public void WhenWorkerEndIsCalledItFiresTheCompletedEvent()
        {
            //Arrange

            var worker = new QWorker<object>(mockProcessor.Object, mockQueue.Object);

            var onCompleteEventFired = false;

            var completeDelegate = new Action<IQWorker>(q => { onCompleteEventFired = true; });

            worker.WorkerCompleted += completeDelegate;

            //Act
            worker.Start();
            System.Threading.Thread.Sleep(10); //Give time to complete processing item

            worker.End();

            System.Threading.Thread.Sleep(10); //Give time to complete processing item

            //Assert
            Assert.IsTrue(onCompleteEventFired);
        }

        [Test]
        public void WhenExceptionThrownItFiresExceptionEvent()
        {
            //Arrange
            mockQueue.Setup(q => q.Dequeue()).Returns(new object());

            var worker = new QWorker<object>(mockProcessor.Object, mockQueue.Object);

            Exception exceptionFired = null;

            var mockException = new Exception("Mock exception");

            var exceptionDelegate = new Action<IQWorker, Exception>((w, e) => { exceptionFired = e; });

            worker.ProcessingException += exceptionDelegate;

            mockProcessor.Setup(p => p.Process(It.IsAny<object>())).Throws(mockException);

            //Act
            worker.Start();
            System.Threading.Thread.Sleep(10);

            //Assert
            Assert.AreEqual(mockException, exceptionFired);
            

        }

        [Test]
        public void DontProcessItemIfNothingReturnsFromTheQueue()
        {
            //Arrange
            mockQueue.Setup(q => q.Dequeue()).Returns(null);

            var worker = new QWorker<object>(mockProcessor.Object, mockQueue.Object);

            //Act
            worker.Start();
            System.Threading.Thread.Sleep(10);

            //Assert
            mockProcessor.Verify(p => p.Process(It.IsAny<object>()), Times.Never);
        }

        [Test]
        public void ProcessItemIfItemReturnsFromTheQueue()
        {
            //Arrange
            mockQueue.Setup(q => q.Dequeue()).Returns(new object());

            var worker = new QWorker<object>(mockProcessor.Object, mockQueue.Object);

            //Act
            worker.Start();
            System.Threading.Thread.Sleep(10);

            //Assert
            mockProcessor.Verify(p => p.Process(It.IsAny<object>()), Times.AtLeastOnce);
        }


        [Test]
        public void AfterTheWorkerHasEndedNoMoreEventsAreFired()
        {
            //Arrange
            mockQueue.Setup(q => q.Dequeue()).Returns(new object());

            var worker = new QWorker<object>(mockProcessor.Object, mockQueue.Object);

            worker.Start();
            System.Threading.Thread.Sleep(10);

            //Act
            worker.End();

            //Assert

        }

    }
}
