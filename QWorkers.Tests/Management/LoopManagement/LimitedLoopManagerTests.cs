﻿using NUnit.Framework;
using QWorkers.Core.Management.LoopManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Tests.Management.LoopManagement
{
    [TestFixture]
    public class LimitedLoopManagerTests
    {
        [Test]
        [Timeout(1000)]
        public void LoopLimitedToIntegerIterations()
        {
            //Arrange
            var loopLimit = 159;
            var loopManager = new LimitedLoopManager(loopLimit);
            var loopCount = 0;

            //Act
            while(loopManager.ShouldContinue)
            {
                loopCount++;
            }

            //Assert
            Assert.AreEqual(loopLimit, loopCount);
        }

        [Test]
        public void LoopShouldNotContinueAfterCancelIsCalled()
        {
            //Arrange
            var loopLimit = 159;
            var loopManager = new LimitedLoopManager(loopLimit);
            var loopCount = 0;

            //Act
            loopManager.Cancel();

            //Assert
            Assert.IsFalse(loopManager.ShouldContinue);
        }

    }
}
