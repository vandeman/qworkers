﻿using Moq;
using NUnit.Framework;
using QWorkers.Core.Management;
using QWorkers.Core.Management.WorkerScalingRules;
using QWorkers.Core.Queues;
using QWorkers.Core.Workers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Tests.Management
{
    [TestFixture]
    public class WorkerManagerTests
    {

        Mock<IQWorkerScalingRule> mockScalingRule;
        Mock<IMyQueue> mockQueue;
        Mock<IQWorker> mockWorker;

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void MonitorSweepStartsNewWorkerWhenIncreaseScalingRuleDemandsIncrease()
        {
            //Arrange
            var manager = new WorkerManager(mockScalingRule.Object, mockWorker.Object, mockQueue.Object);

            mockScalingRule.Setup(sr => sr.Increase(It.IsAny<int>())).Returns(true);

            //Act
            //manager.MonitorSweep();

            //Assert


        }

    }
}
