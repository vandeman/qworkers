﻿using NUnit.Framework;
using QWorkers.Core.Queues;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Tests.Queues
{
    [TestFixture]
    public class MemoryQueueTests
    {

        [Test]
        public void AddingSameItem5Times_QueueCount()
        {
            var q = new MemoryQueue<object>();

            var myObj = new object();

            q.Enqueue(myObj);
            q.Enqueue(myObj);
            q.Enqueue(myObj);
            q.Enqueue(myObj);
            q.Enqueue(myObj);

            Assert.AreEqual(5, q.Count);

        }

        [Test]
        public void AddItemEnqueueIsTheItemDequeued()
        {
            //Arrange
            var q = new MemoryQueue<object>();
            var myObj = new object();


            //Act
            q.Enqueue(myObj);
            var objOut = q.Dequeue();

            //Assert
            Assert.AreEqual(myObj, objOut);
        }

        [Test]
        public void DequeuingEmptyQueueReturnsNull()
        {
            var q = new MemoryQueue<object>(); //empty queue

            var item = q.Dequeue();

            Assert.IsNull(item);
        }

    }
}
