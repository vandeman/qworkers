﻿using NUnit.Framework;
using QWorkers.Core.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Tests.Utility.Buffers
{
    [TestFixture]
    public class CircularBufferTests
    {

        [Test]
        public void InitialisingTheBufferSizeIsReflectedInTheCount()
        {
            //Arrange
            int bufferSize= 12;
            var buffer = new CircularBuffer<int>(bufferSize);

            //Act
            int count = buffer.Count;

            //Assert
            Assert.AreEqual(bufferSize, count);


        }

        [Test]
        public void AddedItemsShouldAppearInTheOrderTheyWereAdded()
        {
            //Arrange
            var buffer = new CircularBuffer<int>(3);

            //Act
            buffer.Add(1);
            buffer.Add(2);
            buffer.Add(3); //Should be full now

            //Assert
            Assert.AreEqual(1, buffer.Item(0));
            Assert.AreEqual(2, buffer.Item(1));
            Assert.AreEqual(3, buffer.Item(2));
        }

        [Test]
        public void WhenBufferIsFullTheNextItemReplacesTheOldestItem()
        {
            //Arrange
            var buffer = new CircularBuffer<int>(3);

            buffer.Add(1);
            buffer.Add(2);
            buffer.Add(3); //Should be full now

            //Act
            buffer.Add(4);

            //Assert
            Assert.AreEqual(4, buffer.Item(0));
            Assert.AreEqual(2, buffer.Item(1));
            Assert.AreEqual(3, buffer.Item(2));
        }

    }
}
