﻿using NUnit.Framework;
using QWorkers.Core.Utility.Buffers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QWorkers.Tests.Utility.Buffers
{
    [TestFixture]
    public class HistoricalBufferTests
    {


        [Test]
        public void InitialisingTheBufferSizeIsReflectedInTheCount()
        {
            //Arrage
            int bufferCapacity = 102;
            var buffer = new HistoricalBuffer<int>(bufferCapacity);

            //Act
            int count = buffer.Count;

            //Assert
            Assert.AreEqual(bufferCapacity, count);
        }

        [Test]
        public void AddedItemsShouldAppearInTheOrderTheyWereAdded()
        {
            //Arrage
            int bufferCapacity = 102;
            var buffer = new HistoricalBuffer<int>(bufferCapacity);

            //Act
            buffer.Add(1);
            buffer.Add(2);
            buffer.Add(3);

            //Assert
            Assert.AreEqual(1, buffer.Item(0));
            Assert.AreEqual(2, buffer.Item(1));
            Assert.AreEqual(3, buffer.Item(2));


        }

        [Test]
        public void WhenBufferIsFullTheNextShiftsTheArray()
        {
            //Arrage full buffer
            int bufferCapacity = 3;
            var buffer = new HistoricalBuffer<int>(bufferCapacity);

            buffer.Add(1);
            buffer.Add(2);
            buffer.Add(3);

            //Act
            buffer.Add(4);

            //Assert
            Assert.AreEqual(2, buffer.Item(0));
            Assert.AreEqual(3, buffer.Item(1));
            Assert.AreEqual(4, buffer.Item(2));



        }
            

    }
}
